FROM registry.gitlab.com/fdroid/ci-images-client
MAINTAINER michael@vonglasow.com

# This installs fdroidserver and everything needed to build packages.
RUN 	test -d build || mkdir build
RUN 	test -d fdroidserver || mkdir fdroidserver
RUN 	git ls-remote https://gitlab.com/fdroid/fdroidserver.git master
RUN 	curl --silent https://gitlab.com/fdroid/fdroidserver/repository/master/archive.tar.gz \
		| tar -xz --directory=fdroidserver --strip-components=1

# $PWD translated from `pwd`
ENV 	PATH="$PWD/fdroidserver:$PATH"
# $PWD translated from $CI_PROJECT_DIR
ENV 	PYTHONPATH="$PWD/fdroidserver:$PWD/fdroidserver/examples"
ENV 	PYTHONUNBUFFERED=true

# TODO debug only
RUN 	echo PATH=$PATH
RUN 	echo PYTHONPATH=$PYTHONPATH

RUN 	bash fdroidserver/buildserver/setup-env-vars $ANDROID_HOME
RUN 	adduser --disabled-password --gecos "" vagrant

# $PWD translated from $CI_PROJECT_DIR
RUN 	ln -s $PWD/fdroidserver /home/vagrant/fdroidserver

RUN 	mkdir -p /vagrant/cache
RUN 	wget -q https://services.gradle.org/distributions/gradle-5.6.2-bin.zip \
			--output-document=/vagrant/cache/gradle-5.6.2-bin.zip
RUN 	bash fdroidserver/buildserver/provision-gradle
RUN 	bash fdroidserver/buildserver/provision-apt-get-install http://deb.debian.org/debian

# running bsenv.sh does not work here, the following lines are its unrolled contents as of Feb 2021
# (might need updating if upstream changes)
ENV 	PATH=$PATH:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:/opt/gradle/bin
ENV 	DEBIAN_FRONTEND=noninteractive

RUN 	apt-get dist-upgrade

# install fdroidserver from git, with deps from Debian, until fdroidserver
# is stable enough to include all the things needed here
RUN 	apt-get install -t stretch-backports \
			fdroidserver \
			python3-asn1crypto \
			python3-ruamel.yaml \
			yamllint
RUN 	apt-get purge fdroidserver

ENV 	GRADLE_USER_HOME=$PWD/.gradle

# NOTE: In order to run `fdroid build --on-server`, you will need to install sudo first.
# F-Droid will uninstall it at the end of the build and you will need to reinstall it in order to
# run the command multiple times.
